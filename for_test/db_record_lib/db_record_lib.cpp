//
// Created by wolp on 18. 8. 24.
//

#include "db_record_lib.h"
db_record_lib::db_record_lib(const std::string & _file_name, int _fourcc, double _frame, cv::Size _size , bool iscolor)
{
    video_Writer = new cv::VideoWriter(_file_name, _fourcc ,_frame , _size , iscolor);
}

void db_record_lib::write(const Mat& image){
    video_Writer->write(image);

}

void db_record_lib::end()
{
    video_Writer->release();
}


db_record_lib *
db_record_lib::Create(const std::string &_file_name, cv::Size _size, int _fourcc, double _frame, bool iscolor) {

    if (_fourcc == 0 )
        _fourcc = h264();
    db_record_lib * temp = new db_record_lib(_file_name, _fourcc , _frame ,_size,  iscolor);
    return temp;
}


int db_record_lib::h264() {
    return CV_FOURCC('H', '2','6','4');
}
