#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/optflow.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <string.h>

#define power(x)((x*x))
#define pythagorean(x,y)( sqrt (power(x) + power (y)))

int main() {

    using namespace cv;

    VideoCapture video(0);
    Mat frame , gray, pre_gray, flow;
    Mat histogram;

    namedWindow("frame");
    namedWindow("historam");

    const int process_scale_inverse = 2;
    video.read(frame);
    resize(frame, frame, frame.size() / process_scale_inverse);

    Size ksize_of_ft(30,30);
    Size size_of_freq(frame.size().width / ksize_of_ft.width + ((frame.size().width % ksize_of_ft.width)? 1 :0 ) ,
                      frame.size().height / ksize_of_ft.height + ((frame.size().height % ksize_of_ft.height)? 1 :0)
    );

    int **  frequency_table = new int *[size_of_freq.width];
    for(int i = 0; i < size_of_freq.width; i++)
    {
        frequency_table[i] = new int [size_of_freq.height];
        memset(frequency_table[i], 0 ,  size_of_freq.height * sizeof(int));
    };


    float **  map_of_flow = new float * [frame.size().width];
    for(int i =0 ; i < frame.size().width; i++)
    {
        map_of_flow[i] = new float [frame.size().height];
    }


    while(true)
    {
        video.read(frame);
        GaussianBlur(frame , frame, Size(9,9) , 1,1);

        cvtColor(frame,gray, cv::COLOR_BGR2GRAY);

        resize(gray, gray, gray.size() / process_scale_inverse);
        if (pre_gray.empty() == false )
        {
            //optflow::calcOpticalFlowSparseToDense(pre_gray, gray , flow);
            calcOpticalFlowFarneback(pre_gray , gray , flow, 0.4 , 1 , 12,  2, 8,1.2 ,0);

            float max = 0.0f;
            float avg = 1;
            const float threshold = 1.0f;
            int count_of_over_threshold =0 ;

            for(int x  = 0 ; x < gray.cols; x += 1)
            {
                for(int y = 0 ; y < gray.rows; y += 1 )
                {
                    Point2f flow_vect = flow.at<Point2f>(Point(x, y));
                    map_of_flow[x][y] = cvRound( pythagorean(flow_vect.x , flow_vect.y));

                    if(map_of_flow[x][y] > max)
                        max = map_of_flow[x][y];
                    avg += map_of_flow[x][y];
                    avg /=2;

                    if (map_of_flow[x][y] > threshold )
                        count_of_over_threshold++;
                }
            }
            int avg_x = 0;
            int avg_y = 0;
            float avg_far_from_center =0 ;

            if (count_of_over_threshold > 400  ) // limit pix is 400
            {
                for(int x  = 0 ; x < gray.cols; x += 1)
                {
                    for(int y = 0 ; y < gray.rows; y += 1 )
                    {

                        if ( map_of_flow[x][y] > 1 ) {
                            avg_x += x;
                            avg_y += y;
                        }


                    }
                }
                avg_x /= count_of_over_threshold;
                avg_y /= count_of_over_threshold;

                for(int i = 0; i < size_of_freq.width; i++)
                {
                    memset(frequency_table[i], 0 ,  size_of_freq.height * sizeof(int));
                };

                for(int x  = 0 ; x < gray.cols; x += 1)
                {
                    for(int y = 0 ; y < gray.rows; y += 1 )
                    {

                        if ( map_of_flow[x][y] > threshold ) {

                            avg_far_from_center += sqrt (power(avg_x - x) + power(avg_y - y));
                            frequency_table[x / ksize_of_ft.width ][y / ksize_of_ft.height]++;
                        }
                    }
                }

                //circle(frame,Point(avg_x, avg_y)* process_scale_inverse ,30, Scalar(255,255,255),10);

                Point LT(frame.size().width,frame.size().height); // left top
                Point RB(0,0); // right bottom
                for(int x = 0 ; x < size_of_freq.width ; x++)
                {
                    for(int y =0 ; y < size_of_freq.height; y++)
                    {
                        if (size_of_freq.width * size_of_freq.height / 2  < frequency_table[x][y])
                        {
                            std::cout << "over than thresold";
                            if (x * ksize_of_ft.width * process_scale_inverse  < LT.x)
                                LT.x = x * ksize_of_ft.width * process_scale_inverse;
                            if (x * ksize_of_ft.width * process_scale_inverse > RB.x )
                                RB.x =  x * ksize_of_ft.width * process_scale_inverse ;
                            if (y * ksize_of_ft.height * process_scale_inverse < LT.y)
                                LT.y = y * ksize_of_ft.height * process_scale_inverse;
                            if (y * ksize_of_ft.height * process_scale_inverse > RB.y)
                                RB.y = y * ksize_of_ft.height * process_scale_inverse ;
                        }


                    }
                }



                rectangle(frame, LT, RB, Scalar(255,0,255), 10);


            }
            histogram = Mat(Size(gray.size().width * gray.size().height, 400), CV_32F).clone();
            resize(histogram, histogram, Size(histogram.cols/ 100,  histogram.rows));
            cv::putText(histogram,std::to_string(max), Point(0,100), FONT_HERSHEY_SIMPLEX , 1, Scalar(255,0,0));
            cv::putText(histogram,std::to_string(avg), Point(0,200), FONT_HERSHEY_SIMPLEX , 1, Scalar(255,0,0));
            cv::putText(histogram,std::to_string(count_of_over_threshold), Point(0,300), FONT_HERSHEY_SIMPLEX , 1, Scalar(255,0,0));
            imshow("histogram",histogram);

            int jump_width = 5;
            for(int x  = 0 ; x < frame.cols; x += jump_width)
            {
                for(int y = 0 ; y < frame.rows; y += jump_width )
                {
                    if (map_of_flow[x/ process_scale_inverse][y / process_scale_inverse]< 1.0f)
                        continue;
                    Point current_pos = Point(x,y);
                    Point2f flow_vect = flow.at<Point2f>(Point(x,  y)/ process_scale_inverse) * 10;
                    line(frame , current_pos,
                        Point( cvRound(x + flow_vect.x ), cvRound(y + flow_vect.y)) , Scalar(255,0,0));
                }
            }
        }

        imshow("frame", frame);
        gray.copyTo(pre_gray);




        if (waitKey(1) == 27 )
        {
            break;

        }
    }

    return 0;
}