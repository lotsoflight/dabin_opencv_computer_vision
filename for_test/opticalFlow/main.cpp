#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/optflow.hpp>
#include <opencv2/imgproc.hpp>
#include <string>

#define power(x)((x*x))

int main() {

    using namespace cv;

    VideoCapture video(0);
    Mat frame , gray, pre_gray, flow;
    Mat histogram;

    namedWindow("frame");
    namedWindow("historam");

    const int process_scale_inverse = 2;
    video.read(frame);
    resize(frame, frame, frame.size() / process_scale_inverse);

    float **  map_of_flow = new float * [frame.size().width];
    for(int i =0 ; i < frame.size().width; i++)
    {
        map_of_flow[i] = new float [frame.size().height];
    }


    while(true)
    {
        video.read(frame);
        cvtColor(frame,gray, cv::COLOR_BGR2GRAY);
        resize(gray, gray, gray.size() / process_scale_inverse);
        if (pre_gray.empty() == false )
        {
            //optflow::calcOpticalFlowSparseToDense(pre_gray, gray , flow);
           calcOpticalFlowFarneback(pre_gray , gray , flow, 0.4 , 1 , 12,  2, 8,1.2 ,0);

            int jump_width = 5;
            for(int x  = 0 ; x < frame.cols; x += jump_width)
            {
                for(int y = 0 ; y < frame.rows; y += jump_width )
                {
                    Point current_pos = Point(x,y);
                    Point2f flow_vect = flow.at<Point2f>(Point(x, y)/ process_scale_inverse) * 10;
                    line(frame , current_pos,
                        Point( cvRound(x + flow_vect.x ), cvRound(y + flow_vect.y)) , Scalar(255,0,0));
                }
            }
            float max = 0.0f;
            float avg = 1;



            for(int x  = 0 ; x < gray.cols; x += 1)
            {
                for(int y = 0 ; y < gray.rows; y += 1 )
                {
                    Point2f flow_vect = flow.at<Point2f>(Point(x, y)/ process_scale_inverse);
                    map_of_flow[x][y] =  sqrt( power(flow_vect.x) + power(flow_vect.y));
                    if(map_of_flow[x][y] > max)
                        max = map_of_flow[x][y];
                    avg += map_of_flow[x][y];
                    avg /=2;
                }
            }
            histogram = Mat(Size(gray.size().width * gray.size().height, 400), CV_32F).clone();
            resize(histogram, histogram, Size(histogram.cols/ 100,  histogram.rows));
            cv::putText(histogram,std::to_string(max), Point(0,100), FONT_HERSHEY_SIMPLEX , 1, Scalar(255,0,0));
            cv::putText(histogram,std::to_string(avg), Point(0,200), FONT_HERSHEY_SIMPLEX , 1, Scalar(255,0,0));
            imshow("histogram",histogram);




        }

        imshow("frame", frame);
        gray.copyTo(pre_gray);




        if (waitKey(1) == 27 )
        {
            break;

        }
    }

    return 0;
}