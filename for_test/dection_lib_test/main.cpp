#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/optflow.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <string.h>
#include "db_dection_box_lib.h"

int main() {
    using namespace cv;
    VideoCapture video(0);
    Mat frame ;
    Point LT;
    Point RB;
    video.read(frame);
    db_dection_box_lib * dection_box_lib = new db_dection_box_lib(frame.size(),2 , Size(30,30), 1, 100, 300);

    namedWindow("video0");
    while(true)
    {
        video.read(frame);

        if( dection_box_lib->process(frame,&LT, &RB))
        {
            rectangle(frame , LT, RB , Scalar(255,0,0), 10);
        }
        imshow("video0", frame);
        if(waitKey(1) == 27) // press ESC
        {
            break;
        }

    }
    return 0;
}