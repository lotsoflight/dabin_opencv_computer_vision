#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include "db_record_lib.h"


int main() {
    using namespace cv;

    cv::VideoCapture video(0);
    cv::Mat frame;
    db_record_lib * recoder = nullptr ;
    video.read(frame);

    recoder =  db_record_lib::Create("sample.mp4",frame.size());

    while(true)
    {

        video.read(frame);
        recoder->write(frame);
        cv::namedWindow("video 0 ",cv::WINDOW_AUTOSIZE);
        cv::imshow("video 0 " ,  frame);
        if (waitKey(1) == 27 )
        {

            recoder->end();
            delete(recoder);
            break;
        }
    }
    return 0;
}