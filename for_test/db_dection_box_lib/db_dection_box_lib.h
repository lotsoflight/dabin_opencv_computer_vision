//
// Created by wolp on 18. 8. 26.
//

#ifndef DB_DECTION_BOX_LIB_DB_DECTION_BOX_LIB_H
#define DB_DECTION_BOX_LIB_DB_DECTION_BOX_LIB_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/optflow.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <string.h>



using namespace cv;
class db_dection_box_lib {

    Mat pre_gray;
    Mat gray;
    Mat flow;

    int proc_scale_invers ;
    Size origin_size;
    Size proc_size;
    Size ksize_of_ft;
    Size ft_size;
    int ** freqence_table;
    float ** flow_map;
    float motion_threshold;
    int count_over_motion_threshold;
    int require_motion_threshold;
    int ft_threshold;





public :
    db_dection_box_lib(Size _origin_size , int _proc_scale_invers , Size _ksize_of_ft,
                       float _motion_threshold , int _ft_threshold, int _require_motion_threshold);
    bool process(Mat frame , Point * LT , Point * RB);

};


#endif //DB_DECTION_BOX_LIB_DB_DECTION_BOX_LIB_H
