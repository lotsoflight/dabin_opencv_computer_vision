//
// Created by wolp on 18. 8. 26.
//

#include "db_dection_box_lib.h"

#define NUM2BIN(x)((x ? 1 : 0 ))
#define power(x)((x*x))
#define pythagorean(x,y)( sqrt (power(x) + power (y)))

db_dection_box_lib::db_dection_box_lib(Size _origin_size, int _proc_scale_invers, Size _ksize_of_ft,
                                       float _motion_threshold, int _ft_threshold, int _require_motion_threshold) {
    origin_size = _origin_size;
    proc_scale_invers = _proc_scale_invers;
    proc_size = _origin_size / proc_scale_invers;

    ksize_of_ft = _ksize_of_ft;
    motion_threshold = _motion_threshold;
    require_motion_threshold = _require_motion_threshold;
    require_motion_threshold = _require_motion_threshold;
    ft_threshold = _ft_threshold;



    ft_size = Size(proc_size.width / ksize_of_ft.width +NUM2BIN(proc_size.width / ksize_of_ft.width ),
                   proc_size.height / ksize_of_ft.height +NUM2BIN(proc_size.height / ksize_of_ft.height ));

    freqence_table = new int * [ft_size.width];
    for (int i = 0 ;  i < ft_size.width ; i++)
    {
        freqence_table[i] = new int[ft_size.height];
        memset(freqence_table[i], 0, ft_size.height * sizeof(int));

    }

    flow_map = new  float * [proc_size.width];
    for(int i = 0 ; i < proc_size.width ; i++)
    {
        flow_map [i] = new float [proc_size.height];
    }



}

bool db_dection_box_lib::process(Mat frame, Point *LT, Point *RB)
{
    resize(frame,gray , proc_size);
    GaussianBlur(gray,gray, Size(9,9), 1,1);
    cvtColor(gray, gray,COLOR_BGR2GRAY );

    if(pre_gray.empty() == false)
    {
        calcOpticalFlowFarneback(pre_gray , gray , flow, 0.4 , 1 , 12,  2, 8,1.2 ,0);
        count_over_motion_threshold = 0;

        for (int i = 0 ;  i < ft_size.width ; i++)
        {
            memset(freqence_table[i], 0, ft_size.height * sizeof(int));
        }
        for(int x  = 0 ; x < proc_size.width; x += 1)
        {
            for (int y = 0; y < proc_size.height; y += 1) {
                Point2f flow_vect = flow.at<Point2f>(Point(x, y));
                flow_map[x][y] = cvRound(pythagorean(flow_vect.x, flow_vect.y));

                if (flow_map[x][y] > motion_threshold)
                {
                    count_over_motion_threshold++;
                    freqence_table[x / ksize_of_ft.width][y / ksize_of_ft.height]++;
                }

            }

        }

        if( count_over_motion_threshold > require_motion_threshold  == false)
        {
            goto cant_find_box;
        }

        LT->x = origin_size.width;
        LT->y = origin_size.height;
        RB->x = 0;
        RB->y = 0;

        int count_of_over_ft = 0;
        for(int x = 0; x < ft_size.width; x++)
        {
            for(int y = 0 ; y< ft_size.height; y++)
            {
                if(freqence_table[x][y] > ft_threshold)
                {
                    count_of_over_ft ++;
                    if(x * proc_scale_invers * ksize_of_ft.width < LT->x)
                        LT->x = x * proc_scale_invers * ksize_of_ft.width;
                    if(x * proc_scale_invers * ksize_of_ft.width > RB->x)
                        RB->x = x * proc_scale_invers * ksize_of_ft.width;
                    if(y * proc_scale_invers * ksize_of_ft.height < LT->y)
                        LT->y = y * proc_scale_invers * ksize_of_ft.height;
                    if(y * proc_scale_invers * ksize_of_ft.height > RB->y)
                        RB->y = y * proc_scale_invers * ksize_of_ft.height;

                }
            }
        }
        if(count_of_over_ft >4)
            goto find_box;



    }


    cant_find_box:
    pre_gray = gray.clone();
    return false;
    find_box:
    pre_gray = gray.clone();
    return true;
}
