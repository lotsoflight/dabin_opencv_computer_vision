//
// Created by wolp on 18. 8. 30.
//

#include "TimeString.h"

std::string TimeString::Get()
{
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer,sizeof(buffer), "%d-%m-%Y %H:%M:%S",timeinfo);
    return std::string(buffer);
}