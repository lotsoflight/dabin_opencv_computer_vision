//
// Created by wolp on 18. 8. 30.
//

#ifndef FINAL_TIMESTRING_H
#define FINAL_TIMESTRING_H

#include <ctime>
#include <iostream>

class TimeString {
    time_t rawtime ;
    struct  tm * timeinfo;
    char buffer[80];
public :
    std::string Get();
};


#endif //FINAL_TIMESTRING_H
