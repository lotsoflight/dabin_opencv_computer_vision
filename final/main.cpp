#include "db_record_lib.h"
#include "db_dection_box_lib.h"
#include "TimeString.h"

int main() {
    VideoCapture video(0);
    Mat frame;
    TimeString time_string;
    Size size;
    Point LT , RB;


    video.read(frame);
    size = frame.size();
    int current = 0;


    namedWindow("video0");
    db_record_lib * record_lib = nullptr;
    db_dection_box_lib * dection_box_lib = new db_dection_box_lib(size , 2 , Size(30,30), 1.0f ,300 ,100 );

    while(true)
    {
        video.read(frame);
        current = dection_box_lib->process(frame,&LT, &RB) ? (current + 1):(0);

        if(current == 1 )
        {
            record_lib = db_record_lib::Create(time_string.Get() + std::string(".mp4"), size);
        }
        if(current >= 1 )
        {
            record_lib->write(frame);
            Mat croped = frame.clone();
            imwrite( "image/"+ time_string.Get() + ".jpg", croped(Rect(LT.x ,LT.y, RB.x - LT.x , RB.y - LT.y)));
            rectangle(frame,LT , RB , Scalar(255,255,255), 10);

        }

        if(current == 0 && record_lib != nullptr)
        {
            record_lib->end();
            delete(record_lib);
            record_lib = nullptr;
        }
        imshow("video0", frame);

        if (waitKey(1) == 27)
            break;

    }

    if(record_lib != nullptr)
    {
        record_lib->end();
        delete(record_lib);
        record_lib = nullptr;
    }
    return 0;
}