//
// Created by wolp on 18. 8. 24.
//

#ifndef UNTITLED_DB_RECORD_LIB_H
#define UNTITLED_DB_RECORD_LIB_H
#include <opencv2/core.hpp>
#include <opencv2/video.hpp>
#include <opencv2/highgui.hpp>
#include <vector>
#include <string>

using namespace cv;
class db_record_lib {

    cv::VideoWriter * video_Writer = nullptr;
private:

    db_record_lib(const std::string & _file_name, int _fourcc, double _frame, cv::Size _size , bool iscolor);

public :
    static db_record_lib * Create(const std::string & _file_name, cv::Size _size ,  int _fourcc  = 0  , double _frame = 30, bool iscolor =  true);
    static int h264();
    void write(const Mat& image);
    void end();


};


#endif //UNTITLED_DB_RECORD_LIB_H
